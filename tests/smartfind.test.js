const app = require("../index.js");
const supertest = require("supertest");
const request = supertest(app);


describe("updateProduct", () => {
  it("should get products", async () => {
    const input = {
      search: "axeite"
    };
    const response = await request.post('/smart-find').send({ input });
    expect(response.status).toBe(200);
    expect(Array.isArray(response.body.products)).toBe(true);
  });
  
});

