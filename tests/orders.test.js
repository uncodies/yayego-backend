const app = require("../index.js");
const supertest = require("supertest");
const request = supertest(app);

// describe("addProduct", () => {
//   it("should add a product to the order", async () => {
//     const data = {
//       user_id: 1,
//       amount: 5,
//       product_id: 9
//     };
//     const response = await request.post('/orders/addProduct').send(data);
//     expect(response.status).toBe(200);
//     expect(response.body.result).toBe('success');
//   });
//   it("should return an error if the user_id is invalid", async () => {
//     const data = {
//       user_id: 2,
//       amount: 5,
//       product_id: 2
//     };
//     const response = await request.post('/orders/addProduct').send(data);
//     expect(response.status).toBe(400);
//   });
// });

describe("updateProduct", () => {
  it("should update a product to the order", async () => {
    const data = {
      user_id: 1,
      amount: 10,
      product_id: 9
    };
    const response = await request.post('/orders/updateProduct').send(data);
    expect(response.status).toBe(200);
    expect(response.body.result).toBe('success');
  });
  it("should return an error if the user_id is invalid", async () => {
    const data = {
      user_id: 2,
      amount: 5,
      product_id: 2
    };
    const response = await request.post('/orders/updateProduct').send(data);
    expect(response.status).toBe(400);
  });
  it("should return an error if the product_id is invalid", async () => {
    const data = {
      user_id: 2,
      amount: 5,
      product_id: 2222
    };
    const response = await request.post('/orders/updateProduct').send(data);
    expect(response.status).toBe(500);
  });
});

// describe("updateProduct", () => {
//   it("should update the details of a product in the order", () => {
//     // Test implementation goes here
//   });

//   it("should return an error if the product does not exist", () => {
//     // Test implementation goes here
//   });
// });