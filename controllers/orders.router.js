const { GraphQLClient, gql } = require("graphql-request");
const { QueryFactory } = require("../functions");

const getLastOrder = gql`
  query LastOrder($customer_id: Int!) {
    order(where: {customer_id: {_eq: $customer_id}}, limit: 1, order_by: {_id:desc}) {
      _id
    }
  }
`

const addProductToOrder = gql`
  mutation InsertProductOnOrder($order_id: Int!, $amount: Int!, $product_id: Int!) {
    insert_order_product_one(object: {order_id: $order_id, product_id: $product_id, amount: $amount}) {
      _id
    }
  }
`

const updateProductToOrder = gql`
  mutation UpdateProductToOrder($order_id: Int!, $amount: Int!, $product_id: Int!) {
    update_order_product(where: {order_id: {_eq: $order_id}, product_id: {_eq: $product_id}}, _set: {amount: $amount}) {
      affected_rows
    }
  }
`

const GetLastOrder = (customer_id) => {
  return QueryFactory(getLastOrder, { customer_id });
};

const AddProductToOrder = (order_id, amount, product_id) => {
  return QueryFactory(addProductToOrder, { order_id, amount, product_id });
};

const UpdateProductToOrder = (order_id, amount, product_id) => {
  return QueryFactory(updateProductToOrder, { order_id, amount, product_id });
};

const addProduct = async ({ body: { user_id, amount, product_id } }, res) => {
  try {
    // Parse parameters to integers
    user_id = parseInt(user_id);
    amount = parseInt(amount);
    product_id = parseInt(product_id);

    // Check if parameters are valid integers
    if (isNaN(user_id) || isNaN(amount) || isNaN(product_id)) {
      return res.status(400).send({ result: 'error', message: 'Invalid parameters' });
    }

    console.log(user_id, amount, product_id)
    const lastOrderResponse = await GetLastOrder(user_id);
    if(!lastOrderResponse.order.length) return res.status(404).send({ result: 'error', message: 'No orders found' });

    const addProductResponse = await AddProductToOrder(lastOrderResponse.order[0]._id, amount, product_id);
    if(!addProductResponse.insert_order_product_one._id) return res.status(400).send({ result: 'error', message: 'Product not added' });

    return res.status(200).send({ success: true });
  } catch (error) {
    console.error(error); // Log the error for debugging purposes
    res.status(500).send({ result: 'error', message: 'An error occurred' });
  }
}

const updateProduct = async (req, res) => {
  try {
    const { user_id, amount, product_id } = req.body;
    const lastOrderResponse = await GetLastOrder(user_id);
    if(!lastOrderResponse.order.length) return res.status(400).send({ result: 'error', message: 'No orders found' });
    const updateProductResponse = await UpdateProductToOrder(lastOrderResponse.order[0]._id, amount, product_id);
    console.log(updateProductResponse);
    if(!updateProductResponse.update_order_product.affected_rows === 0) return res.status(400).send({ result: 'error', message: 'Product not updated' });
    return res.status(200).send({ result: 'success' });
  } catch (error) {
    res.status(500).send({ result: 'error', message: error.message });
  }
}

module.exports = {
  addProduct,
  updateProduct,
  GetLastOrder,
  AddProductToOrder,
  QueryFactory,
  getLastOrder
}