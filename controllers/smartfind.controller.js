const { default: axios } = require("axios");


const smartFind = async (req, res) => {
  try {
    const { search } = req.body.input;
    const { data } = await axios.post("https://yayego.uncodie.com/flow/smart_product_find", { "user_input": search });
    return res.status(200).send({ products: data });
  } catch (error) {
    console.log("error", error);
    res.status(400).send({ error: error.message || error });
  }
}

module.exports = {
  smartFind
}