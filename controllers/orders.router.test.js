const app = require("../index.js");
const supertest = require("supertest");
const request = supertest(app);
require('dotenv').config()
const { addProduct, GetLastOrder, AddProductToOrder } = require('../controllers/orders.router'); // Assuming addProduct is exported from orders.router.js
const { QueryFactory } = require('../functions');


// Mocking the 'orders.router' module using Jest.
// This replaces the actual module with a mock module for testing purposes.
// This way, we can simulate the behavior of the 'orders.router' module without having to use the actual module.
// This is useful for isolating the code under test and for testing how the code interacts with its dependencies.


jest.mock('../functions');

beforeEach(() => {
  QueryFactory.mockClear();
});

describe('GetLastOrder', () => {
  it('should call QueryFactory with correct parameters', async () => {
  // Arrange
  const mockCustomerId = 1;
  QueryFactory.mockReturnValueOnce({ order: [ { _id: 21 } ] });

  // Act
  const result = await GetLastOrder(mockCustomerId);
  // Assert
  console.log(result)
  expect(result).not.toEqual({});
  expect(QueryFactory).toHaveBeenCalledTimes(1);
});
});

// jest.mock('../controllers/orders.router');

// describe('addProduct', () => {
//   it('should return 200 if product is added successfully', async () => {
//     // GetLastOrder.mockResolvedValue({ order: [{ _id: '123' }] });
//     // AddProductToOrder.mockResolvedValue({ insert_order_product_one: { _id: '456' } });

//     const res = await request
//       .post('/orders/addProduct')
//       .send({ user_id: '1', amount: 2, product_id: '6' });

//     console.log(res)
//     expect(res.statusCode).toEqual(200);
//     expect(res.body).toEqual({ success: true });
//   });
// });

//   it('should return 400 if no orders found', async () => {
//     GetLastOrder.mockResolvedValue({ order: [] });

//     const res = await request
//       .post('/orders/addProduct')
//       .send({ user_id: '1', amount: 2, product_id: '3' });

//     expect(res.statusCode).toEqual(400);
//     expect(res.body).toEqual({ result: 'error', message: 'No orders found' });
//   });

//   // Add more test cases as needed
// });