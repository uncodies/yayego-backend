var express = require("express");
const { smartFind } = require("../controllers/smartfind.controller");
var router = express.Router();

router.post("/smart-find", smartFind);
module.exports = router;
