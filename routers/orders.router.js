var express = require("express");
var router = express.Router();
const { addProduct, updateProduct } = require("../controllers/orders.router.js");

router.post("/orders/addProduct", addProduct);
router.post("/orders/updateProduct", updateProduct);
module.exports = router;
