const compile = require('./orders.router.js');
const smartRequest = require('./samart.request.route.js');

module.exports = {
    compile,
    smartRequest
};
