const express = require('express');
const app = express();
var bodyParser = require('body-parser');
const { compile, smartRequest } = require("./routers");
app.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
require('dotenv').config()
app.use(bodyParser.json())
app.use("/", compile);
app.use("/", smartRequest);
module.exports = app;
