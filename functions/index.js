const { GraphQLClient } = require("graphql-request");

async function QueryFactory(query, variables) {
  try {
    const graphQLClient = new GraphQLClient(process.env.HASURA_END_POINT);
    const headers = {
      "Content-Type": "application/json",
      "x-hasura-admin-secret": process.env.HASURA_TOKEN,
    };
    const data = await graphQLClient.request(query, variables, headers);
    return data;
  } catch (e) {
    console.error(e);
    return e;
  }
}

module.exports = { QueryFactory };